extends KinematicBody2D

class_name Enemy

export(int) var speed      := 100
export(int) var hp         := 10
export(int) var enemy_dmg  := 5
export(int) var enemy_type := 1

var player
var mapnav
var spb

var first_iter_point : Vector2
var moving           : bool    = false
var stop             : bool    = false

var rng = RandomNumberGenerator.new()

var nx
var ny

var atk_type : String

### Variables for tween
var normal_pos   : Vector2
var normal_ang   : float
var normal_mod   : Color
var attacked     : bool
var damaged      : bool

signal enemy_dead()

func _setup(hero, navigation, sec_per_beat) -> void:
	player     = hero
	mapnav     = navigation
	spb        = sec_per_beat
	hp        *= enemy_type
	enemy_dmg *= enemy_type

func _ready():
	self.get_node("AnimatedSprite").speed_scale = 4 / spb
	
	$Timer.wait_time = spb * enemy_type
	$Timer.start()
	
	_update(player.get_global_position())

func _physics_process(delta):
	var start = self.get_global_position()
	var dest  = mapnav.get_closest_point(player.get_global_position())
	var path  = mapnav.get_simple_path(start, dest)
	
	var mov_dist = speed*delta
	
	if hp <= 0:
		_die()
	
	if stop:
		nx = 0
		ny = 0
	
	for _i in range(path.size()):
		randomize()
		
		var next     = start.distance_to(path[0])
		var dist_vec = path[0] - start
		
		var x_allow
		var y_allow
		
		if nx > 0 and ny > 0:
			x_allow = rng.randi_range(0, 1)
			y_allow = not x_allow
		elif nx == 0 and ny > 0:
			x_allow = false
			y_allow = true
		elif nx > 0 and ny == 0:
			x_allow = true
			y_allow = false
		elif nx <= 0 and ny <= 0:
			x_allow = false
			y_allow = false
		
		if mov_dist <= next and mov_dist >= 0.0 and moving:
			if x_allow and dist_vec.x < 0:
				# warning-ignore:return_value_discarded
				move_and_collide(Vector2.LEFT * 16)
				$AnimatedSprite.flip_h = true
				nx -= 1
			elif x_allow and dist_vec.x > 0:
				# warning-ignore:return_value_discarded
				move_and_collide(Vector2.RIGHT * 16)
				$AnimatedSprite.flip_h = false
				nx -= 1
			elif y_allow and dist_vec.y < 0:
				# warning-ignore:return_value_discarded
				move_and_collide(Vector2.UP * 16)
				ny -= 1
			elif y_allow and dist_vec.y > 0:
				# warning-ignore:return_value_discarded
				move_and_collide(Vector2.DOWN * 16)
				ny -= 1
			
			break
		
		start = path[0]
		path.remove(0)
	
	moving = false

func _update(player_pos : Vector2):
	var player_dist : Vector2
	
	first_iter_point = self.get_position()
	player_dist      = player_pos - first_iter_point
	nx               = abs(floor(player_dist.x/16))
	ny               = abs(floor(player_dist.y/16))
	
	if player_dist.x < 0 and player_dist.y < 0:
		atk_type = "NXNY"
	elif player_dist.x < 0 and player_dist.y > 0:
		atk_type = "NXPY"
	elif player_dist.x > 0 and player_dist.y < 0:
		atk_type = "PXNY"
	elif player_dist.x > 0 and player_dist.y > 0:
		atk_type = "PXPY"

func _stop_moving() -> void:
	stop = true

### Can move again
func _back_to_normal() -> void:
	self._update(player.get_global_position())
	stop = false

func _on_Timer_timeout():
	moving = true
	
	if stop:
		_atk(atk_type)
	
	$Timer.start()

func _take_dmg(dmg : int):
	if !attacked:
		var dest_pos : Vector2
		var dest_ang : float
		var cur_pos  : Vector2 = self.get_global_position()
		var cur_ang  : float   = self.rotation_degrees
		var cur_mod  : Color   = self.modulate
		
		normal_pos = cur_pos
		normal_ang = cur_ang
		normal_mod = cur_mod
		
		if $AnimatedSprite.is_flipped_h():
			dest_pos = cur_pos + Vector2(0, 16)
			dest_ang = 45
		else:
			dest_pos = cur_pos + Vector2(-16, 0)
			dest_ang = -45
		
		$Tween.interpolate_property(self, "global_position", cur_pos, dest_pos, 0.2)
		$Tween.interpolate_property(self, "rotation_degrees", cur_ang, dest_ang, 0.2)
		$Tween.interpolate_property(self, "modulate", cur_mod, Color(1, 1, 1, 0), 0.2)
		$Tween.start()
		
		hp     -= dmg
		damaged = true

func _atk(atk_dir : String):
	if !damaged:
		var dest_pos : Vector2
		var dest_ang : float
		var cur_pos  : Vector2 = self.get_global_position()
		var cur_ang  : float   = self.rotation_degrees
		
		normal_pos = cur_pos
		normal_ang = cur_ang
		attacked   = true
		
		match atk_dir:
			"NXNY":
				dest_pos = cur_pos + Vector2(-24, -24)
				dest_ang = -45
			"NXPY":
				dest_pos = cur_pos + Vector2(-24, 24)
				dest_ang = -135
			"PXNY":
				dest_pos = cur_pos + Vector2(24, -24)
				dest_ang = 45
			"PXPY":
				dest_pos = cur_pos + Vector2(24, 24)
				dest_ang = 135
		
		$Tween.interpolate_property(self, "global_position", cur_pos, dest_pos, 0.2)
		$Tween.interpolate_property(self, "rotation_degrees", cur_ang, dest_ang, 0.2)
		$Tween.start()

func _die() -> void:
	emit_signal("enemy_dead")
	self.call_deferred("free")

func _on_Tween_tween_all_completed():
	if attacked:
		var cur_pos : Vector2 = self.get_global_position()
		var cur_ang : float   = self.rotation_degrees
		
		attacked = false
		
		$Tween.interpolate_property(self, "global_position", cur_pos, normal_pos, 0.2)
		$Tween.interpolate_property(self, "rotation_degrees", cur_ang, normal_ang, 0.2)
		$Tween.start()
	elif damaged:
		var cur_ang : float = self.rotation_degrees
		var cur_mod : Color = self.modulate
		
		damaged = false
		
		$Tween.interpolate_property(self, "rotation_degrees", cur_ang, normal_ang, 0.2)
		$Tween.interpolate_property(self, "modulate", cur_mod, Color(1, 1, 1, 1), 0.2)
		$Tween.start()

func _on_Hit_body_entered(body):
	if body.is_in_group("Hero"):
		body._take_dmg(enemy_dmg, atk_type)
