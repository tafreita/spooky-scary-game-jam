extends AudioStreamPlayer

class_name WaveMusic

export(int)   var bpm

var time               : float
var song_pos_in_beats  : int
var sec_per_beat       : float
var last_reported_beat : int
var beats_before_start : int
var speed              : float
var screenSize         : Vector2 = OS.window_size

signal create_note()

func _ready():
	sec_per_beat = 60.0/bpm
	
	# warning-ignore:return_value_discarded
	$Start.connect("timeout", self, "_on_Start_timeout")

func _physics_process(_delta):
	if self.playing:
		time = self.get_playback_position() + AudioServer.get_time_since_last_mix()
		time -= AudioServer.get_output_latency()
		song_pos_in_beats = int(floor(time/sec_per_beat)) + beats_before_start
		_report_beat()

func _report_beat():
	if last_reported_beat < song_pos_in_beats:
		last_reported_beat = song_pos_in_beats
		emit_signal("create_note")

func _play_with_offset(off):
	beats_before_start = off
	last_reported_beat = 0
	$Start.wait_time = sec_per_beat
	$Start.start()

func _on_Start_timeout():
	song_pos_in_beats += 1
	
	if song_pos_in_beats < beats_before_start - 1:
		$Start.start()
	elif song_pos_in_beats == beats_before_start - 1:
		$Start.wait_time = $Start.wait_time - (AudioServer.get_time_to_next_mix() + AudioServer.get_output_latency())
		$Start.start()
	else:
		self.play()
		$Start.stop()
	
	_report_beat()

func _get_spb() -> float:
	return 60.0/bpm

func _get_wait_time() -> float:
	return (60.0/bpm) - (AudioServer.get_time_to_next_mix() + AudioServer.get_output_latency())
