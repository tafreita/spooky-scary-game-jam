extends KinematicBody2D

export(PackedScene) var weapon_scene 
export(int)  var hp := 100

var attacked : bool = false
var damaged  : bool = false
var can_mov  : bool = true

var instructions : Array = [Vector2.RIGHT, Vector2.LEFT, Vector2.UP, Vector2.DOWN, "Attack", Vector2.ZERO]
var cur_inst     : int = 5

onready var flipped  : bool = $AnimatedSprite.is_flipped_h()

var direction : Vector2

signal update_hero_pos()
signal hero_damaged()
signal dead()

func _setup(time : float) -> void:
	$Timer.wait_time = time

func _physics_process(_delta):
	match cur_inst:
		0:
			if can_mov:
				direction = instructions[cur_inst]
				can_mov = false
				$AnimatedSprite.flip_h = false
				$Timer.start()
				emit_signal("update_hero_pos")
		1:
			if can_mov:
				direction = instructions[cur_inst]
				can_mov = false
				$AnimatedSprite.flip_h = true
				$Timer.start()
				emit_signal("update_hero_pos")
		2:
			if can_mov:
				direction = instructions[cur_inst]
				can_mov = false
				$Timer.start()
				emit_signal("update_hero_pos")
		3:
			if can_mov:
				direction = instructions[cur_inst]
				can_mov = false
				$Timer.start()
				emit_signal("update_hero_pos")
		4:
			if !attacked:
				var weapon = weapon_scene.instance()
				
				weapon.connect("dmg", self, "_on_dmg")
				
				self.add_child(weapon)
				
				attacked = true
		5:
			direction = instructions[cur_inst]
	
	# warning-ignore:return_value_discarded
	move_and_slide(direction*64)

func _on_DeadArea_body_entered(body):
	if body.is_in_group("Enemy"):
		body._stop_moving()

func _on_DeadArea_body_exited(body):
	if body.is_in_group("Enemy"):
		body._back_to_normal()

func _on_dmg(atk, node):
	if node.is_in_group("Enemy"):
		node._take_dmg(atk)

func _mov_allow(allow : bool):
	can_mov = allow
	
	if !allow:
		attacked = false 

func _take_dmg(_dmg : int, atk_type : String) -> void:
	if !damaged:
		var dest_pos : Vector2
		var dest_ang : float
		var cur_ang  : float = self.rotation_degrees
		var cur_mod  : Color = self.modulate
		
		if atk_type[0] == "P":
			dest_pos = self.get_global_position() + Vector2(16, 0)
			dest_ang = 45
		elif atk_type[0] == "N":
			dest_pos = self.get_global_position() + Vector2(-16, 0)
			dest_ang = -45
		
		$Tween.interpolate_property(self, "global_position", self.get_global_position(), dest_pos, 0.2)
		$Tween.interpolate_property(self, "rotation_degrees", cur_ang, dest_ang, 0.2)
		$Tween.interpolate_property(self, "modulate", cur_mod, Color(1, 1, 1, 0), 0.2)
		$Tween.start()
		
		damaged = true
		
		emit_signal("hero_damaged")

func _on_mov(inst : int, allow : bool) -> void:
	cur_inst = inst
	_mov_allow(allow)

func _die() -> void:
	emit_signal("dead")
	self.call_deferred("free")

func _on_Tween_tween_all_completed():
	if damaged:
		var cur_ang : float = self.rotation_degrees
		var cur_mod : Color = self.modulate
		
		$Tween.interpolate_property(self, "rotation_degrees", cur_ang, 0, 0.2)
		$Tween.interpolate_property(self, "modulate", cur_mod, Color(1, 1, 1, 1), 0.2)
		$Tween.start()
		
		damaged = false

func _on_Timer_timeout():
	cur_inst = 5
	can_mov  = true
	attacked = false

func _get_hp() -> int:
	return hp
