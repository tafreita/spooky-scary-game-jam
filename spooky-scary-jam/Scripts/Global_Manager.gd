extends Node2D

export(PackedScene) var menu_scene
export(PackedScene) var game_scene

func _ready():
	var menu = menu_scene.instance()
	
	menu.set_name("Menu")
	menu.connect("go_to_game", self, "_on_go_to_game")
	
	self.add_child(menu)

func _on_go_to_game():
	var game = game_scene.instance()
	
	$Menu.call_deferred("free")
	
	game.set_name("Game")
	game.connect("game_over", self, "_on_game_over")
	
	self.add_child(game)

func _on_game_over():
	var menu = menu_scene.instance()
	
	$Game.call_deferred("free")
	
	menu.set_name("Menu")
	menu.connect("go_to_game", self, "_on_go_to_game")
	
	self.call_deferred("add_child", menu)
